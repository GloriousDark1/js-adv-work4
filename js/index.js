let urlink = 'https://ajax.test-danit.com/api/swapi/films'
let div = document.querySelector('#root')

function sendRequest(url) {
	return fetch(url).then(response => response.json())
}

let info = sendRequest(urlink);

function show(item) {
	item.then((data) => {
		const films = data.map((el) => {
			// console.log(el)
			const li = document.createElement('li');
			const episodeId = document.createElement('p');
			const filmName = document.createElement('h1');
			const openingCrawl = document.createElement('p');

			el.characters.forEach((elem) => {
				fetch(elem).then(response => response.json()).then((data) => {
					const person = document.createElement('p');
					person.textContent = data.name;
					filmName.after(person);
					console.log(person)
				})
			})
			episodeId.textContent = el.episodeId;
			filmName.textContent = el.name;
			openingCrawl.textContent = el.openingCrawl;
			li.append(episodeId, filmName, openingCrawl);
			return li;
		})
		const list = document.createElement('ul');
		list.append(...films);
		div.append(list);
	})
}

show(info)